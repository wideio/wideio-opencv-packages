		if (serialization=="json"):
			return simplejson.dumps(data)
		elif (serialization=="bson"):
			return bson.BSON.encode(data)
		else:
			return data

	except Exception,e:
		print "EXCEPTION",e
		raise e

if __name__=="__main__":
  mode="request"
  if (sys.argv>1):
    mode=sys.argv[1]
  if mode=="request":
    request=bson.BSON(open("request/request.bson").read()).decode()
    print function(request)
  if mode=="server":
    port = int (sys.argv[2])
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect(("127.0.0.1", port))
    run_server(s,function)
