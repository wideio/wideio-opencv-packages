#!/usr/bin/env python
from __future__ import print_function
import cv2
import os, sys, re, string
from hdr_parser import *
from zipfile import ZipFile
#from api import add_package_via_API
import shutil

sys.path.append("/home/vh/pro/wideio-API/high_level/")

#from WideioAPI import wideio

class CreateOpencvPackage(CppHeaderParser):
    def get_selected_functions(self, decls):
        for d in decls:	
            if d[0].find("const") > -1 or d[0].find("class") > -1:
		continue
            ok = True
            for a in d[3]:
		if not a[0] in ["Mat","String","Size","double","int"]:
			if a[2] == "":
				ok = False # has no default value
            if ok:
		yield d

    def create_package(self, decls):        
        fheader = open("header.py","r")
        header = fheader.read()
        ffooter = open("footer.py","r")
        footer = ffooter.read()
        fheader.close()
        ffooter.close()

        local_path = os.getcwd()
                
        convert_types = ["int","float"]
        available_types = ["int","double","retval"]
	for d in self.get_selected_functions(decls):		
		name = d[0].split(" ")[0][3:]
		try:
			os.mkdir("wideio-opencv-"+name)
			f = open("wideio-opencv-"+name+"/exe.py","w")
		        f.write(header)
		        doc = eval("cv2." + name  + ".__doc__").split("->")
		        instances = ""
		        args = ""
		        output_left = ""
                        output = []
                        img = None
                        preprocess = ""
                        declarations = "	serialization = None\n"
                        test_params = ""

			for a in d[3]:
		            if a[0] == "Mat" and a[1] == "src":
                                preprocess = preprocess + '	src=numpy.asarray(PIL.Image.open("request/image.png"))\n'
                                preprocess = preprocess + '		src=src.astype(numpy.uint8)\n'
		                args = args + "cv2.cv.fromarray(src),"

		            if a[0] == "Mat" and a[1] in ["img","image"]:
                                preprocess = preprocess + '	img=numpy.asarray(PIL.Image.open("request/image.png"))\n'
                                preprocess = preprocess + '		img=img.astype(numpy.uint8)\n'
		                args = args + "cv2.cv.fromarray(img),"

		            else:
		                if type(a[3]) == list and len(a[3])>0 and a[3][0] == "/O": #output                                                        
		                    if doc[1].find(a[1]) > -1: #return by the left
		                        output_left = output_left + a[1] + ","
                                        output.append(a[1])
		                    elif a[1] == "dst":
		                        instances = instances + "dst = src[:,:]"
		                        args = args + "cv2.cv.fromarray(dst),"
		                else:
		                    if a[0] in available_types:
		                        if type(a[3]) == list and len(a[3])>0 and a[3][0] == "/O": #output
		                            doc = eval("cv2." + name  + ".__doc__").split("->")
		                            
		                            if doc[0].find(a[3]) > -1: #return by the right
                                                declarations = declarations + "	" + a[1] + " = None\n"
		                                args = args + a[1] + ","                                
		                            else: # return by the left
		                                output_left = output_left + a[3] + ","
                                                output.append(a[1])
		                        elif a[2] != "":
		                            continue
		                        else:
		                            test_params = test_params + '		if (request["parameters"].has_key("'+a[1]+'")):\n'
		                            test_params = test_params + '		    '+ a[1] + '='+convert_types[available_types.index(a[0])] +'(request["parameters"]["'+a[1]+'"])\n'
		                            args = args + a[1] + ","
                                            declarations = declarations + "	" + a[1] + " = None\n"


                        f.write(declarations)
			f.write('	if (request.has_key("parameters")):\n')
			f.write('		if (request["parameters"].has_key("serialization")):\n')
			f.write('			serialization=request["parameters"]["serialization"]\n')

                        f.write(test_params)
		        f.write("	try:\n")

		        f.write("	" + instances+"\n")
		        f.write("	" + preprocess)

		        f.write("		doc = pydoc.render_doc(cv2."+name+")\n")
		        if len(output_left) == 0:
		            f.write("		cv2."+name+"("+args[:len(args)-1]+")\n")
		        else:
		            f.write("		"+output_left[:len(output_left)-1]+ " = cv2."+name+"("+args[:len(args)-1]+")\n")

                        for o in output:
                            f.write("		data.append("+o+")\n")
                            
                        f.write(footer)
			f.close()
                        
                        os.chdir(local_path + "/wideio-opencv-"+name)
                        shutil.copy("../generic.py","generic.py")
                        with ZipFile('../wideio-opencv-'+name+'.zip', 'w') as myzip:
                            myzip.write("exe.py")
                            myzip.write("generic.py")
                            
                        os.chdir(local_path)
                        

                        #wideio.addAlgorithm(name,'wideio-opencv-'+name+'.zip',"media/opencv-logo.png",name,name,'[]')
                        #add_package_via_API(name,doc,doc,"media/opencv-logo.png",'wideio-opencv-'+name+'.zip')
      
                except KeyboardInterrupt as e:
                    return
		except Exception as e:
                    print(e)

                    try:
                        os.remove("wideio-opencv-"+name+"/exe.py")
                    except:
                        pass

                    try:
                        os.remove("wideio-opencv-"+name+"/generic.py")
                    except:
                        pass
                    try:
                        os.rmdir("wideio-opencv-"+name)
                    except:
                        pass



    def print_selected_functions(self, decls,str_ = "erode"):
	for d in self.get_selected_functions(decls):
            if d[0].find(str_) == -1:
                continue
            print(d[0], d[1], ";".join(d[2]))
            for a in d[3]:
                print("   ", a[0], a[1], a[2], end="")
                if a[3]:
                    print("; ".join(a[3]))
                else:
                    print()

if __name__ == '__main__':
    parser = CreateOpencvPackage()
    decls = []
    for hname in opencv_hdr_list:
        decls += parser.parse(hname)
    #for hname in sys.argv[1:]:
        #decls += parser.parse(hname, wmode=False)
    #parser.print_decls(decls)
    #parser.print_selected_functions(decls,"erode")
    parser.create_package(decls)
    #print(len(decls))
