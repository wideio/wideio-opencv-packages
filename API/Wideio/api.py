import urllib,urllib2,httplib,pycurl
import sys
import json
import numpy
import os

username = "vh@wide.io"
password = "f4$586!7%s53*_)3"
site_url = "https://beta.wide.io/"

class Storage:
    def __init__(self):
        self.contents = ''
        self.line = 0

    def store(self, buf):
        self.line = self.line + 1
        self.contents = self.contents + buf

    def __str__(self):
        return self.contents

class API():
	def __init__(self,url):
		global username
		global password
		self.retrieved_body = Storage()
		self.f = None
		self.c = pycurl.Curl()

		self.c.setopt(self.c.URL, str(site_url+url))
		self.c.setopt(pycurl.SSL_VERIFYPEER, 0)
		self.c.setopt(pycurl.SSL_VERIFYHOST, 0)
		self.data = None
		self.args = [
			("json", "1"),
			("ajax", "1")
			]

		self.c.setopt(pycurl.USERPWD,username + ':' + password)

	def setArgFile(self,location,name,field="file"):
		self.args.append((str(field), (pycurl.FORM_FILE,str(location))))
		self.args.append(("name", str(name)))

	def toFileRequest(self,name):
		self.f = open(name,"w")
		self.c.setopt(self.c.WRITEDATA,self.f)

	def setArgs(self,args):
		for arg in args:
			self.args.append(arg)
		
	def send(self):
		if self.f == None:
			self.c.setopt(self.c.WRITEFUNCTION, self.retrieved_body.store)
		self.c.setopt(self.c.HTTPPOST, self.args)
		self.c.perform()
		self.c.close()
		if self.f != None:
			self.f.close()
		else:
			self.data=str(self.retrieved_body.contents)


	def getJson(self):
		return json.loads(self.data)
		#raise Exception("Not Json formated answer")
	
	def getFile(self):
		return self.f

d = [
{'type': 'file', 'name': 'image_input', 'value': None, 'required': True},
{'type': 'int', 'name': 'hessianThreshold', 'value': None, 'required': True},
{'type': 'int', 'name': 'nOctaves', 'value': 4, 'required': False},
{'type': 'int', 'name': 'nOctaveLayers', 'value': 2, 'required': False},
{'type': 'bool', 'name': 'extended', 'value': True, 'required': False},
{'type': 'bool', 'name': 'upright', 'value': False, 'required': False},
{'type': 'numpy.ndarray', 'name': 'mask', 'value': None, 'required': False,'shape':(1,1)},
{'type': 'bool', 'name': 'useProvidedKeypoints', 'value': False, 'required': False},
{'type': 'bool', 'name': 'image_output', 'value': False, 'required': False},
{'type': 'bool', 'name': 'cross_circle', 'value': True, 'required': False}]

e = '[{"required": true, "type": "int", "name": "hessianThreshold", "value": null}, {"required": false, "type": "int", "name": "nOctaves", "value": 4}, {"required": false, "type": "int", "name": "nOctaveLayers", "value": 2}, {"required": false, "type": "bool", "name": "extended", "value": true}, {"required": false, "type": "bool", "name": "upright", "value": false}, {"required": false, "type": "numpy.ndarray", "name": "mask", "value": null}, {"required": false, "type": "bool", "name": "useProvidedKeypoints", "value": false}, {"required": false, "type": "bool", "name": "image_output", "value": false}, {"required": false, "type": "bool", "name": "cross_circle", "value": true}]'

class Request():
	def __init__(self,algorithm):
		self.algorithm = algorithm
		self.ready = False
		self.attr_ok = True
		self.access_key = None
		self.res = None
		self.requestid = None
		self.args = []
		self.files = []
		
		for parameter in self.algorithm.parameters:
			self.__dict__[parameter.name] = parameter.value
		
		self.parameters = {}
		
	def __setattr__(self,name,value):
		if name in ["algorithm","requestid","isReady","args"]:
			self.__dict__[name] = value
			return True
		for parameter in self.algorithm.parameters:
			if parameter.name == name:
				# bypass errors like int = None
				if value == parameter.default_value:
					break
				# if file: check if the path is ok
				elif type(value) == str and parameter.param_type == file:
					if os.path.isfile(value) == False:
						print "Bad file path for parameters " + parameter.name + "."
						self.attr_ok = False
						return
				# if int,float,str,bool,etc...	: check if same type
				elif type(value) != parameter.param_type:
					print "Bad type for parameter " + parameter.name + "."
					self.attr_ok = False
					return
				# if numpy.ndarray : check if same shape FIXME : bad condition
				elif type(value) == parameter.param_type and type(value) == numpy.ndarray and value.shape != parameter.shape:
					print "Bad type for parameter " + parameter.name + "."
					self.attr_ok = False
					return
		self.__dict__[name] = value
		return True

	def execute(self):
		args = [("algorithm" , str(self.algorithm.id))]
		# check if all required parameters has been filled.
		for parameter in self.algorithm.parameters:
			# file case
			if parameter.param_type == file:
				if type(self.__dict__[parameter.name]) == str:
					req = API(URLS["addFile"])
					req.setArgFile(self.__dict__[parameter.name],"input")
					req.send()
					result = req.getJson()
					args.append(("associated_file",str(result["res"])))
				elif parameter.required:
					print "Parameter " + parameter.name + " must be set as file location."
					return
			# other cases
			elif parameter.required:
				if self.__dict__[parameter.name] == None:
					print "Parameter " + parameter.name + " has to be filled."
					return

		req = API(URLS["addJob"])
		req.setArgs(args)
		req.send()
		self.__dict__.update(req.getJson())
		wideio.myrequests.append(self)

	def isReady(self):
		req = API(URLS["viewJob"]+ self.res + "/")
		req.setArgs([("access_key",str(self.access_key))])
		req.send()
		result = req.getJson()
		self.__dict__.update(result)

		if int(result["status"]) == 2:
			self.ready = True
			return True
		return False

	def getFile(self,name,output):
		if not self.ready and not self.isReady():
			print "Request not finished yet... Try later"
			return
		
		req = API(URLS["getResult"]+ self.requestid + "/")
		req.setArgs([
				("access_key",str(self.access_key)),
				("filename",str(output))
				])
		req.toFileRequest(output)
		req.send()
	

	def getAvailableFiles(self):
		if not self.ready and not self.isReady():
			print "Request not finished yet... Try later"
			return
		return self.files
class Parameter():
	def __init__(self,param):
		self.name = param["name"]
		self.param_type = Types[param["type"]]
		self.value = param["value"]
		self.default_value = param["value"]
		self.required = param["required"]
		if self.param_type == numpy.ndarray:
			self.shape = param["shape"]
		
class Algorithm():
	def __init__(self,json):	
		for attr in json:
			if attr != 'parameters':
				setattr(self,attr,json[attr])
		self.parameters = []
		for parameter in d:#json['parameters']:
			self.parameters.append(Parameter(parameter))

	def __call__(self,**kwargs):
		request = Request(self)
		for key,value in kwargs.iteritems():
			setattr(request,key,value)
		if not request.attr_ok:
			print "Exiting"
			return
		
		request.execute()
		request.isReady()
		return request.res
		
class Problem():
	def __init__(self,id):
		self.id = id
		
class Wideio():
	def __init__(self,username,password):
		self.username = username
		self.password = password		
		self.myrequests = []
		
		#algorithm
		req = API(URLS["listAlgorithm"])
		req.send()
		for algorithm in req.getJson():
			setattr(self,algorithm["name"],Algorithm(algorithm))
		
		#problems
		"""req = RequestAPI("aaaa","aaaa","compute/problems/list/")
		req.send()
		for algorithm in req.GetJson():
			setattr(self,algorithm["name"],Algorithm(id))"""

	def status(self,id):
		for request in self.myrequests:
			if request.res == id:
				request.isReady()
				return request.status

	def result(self,id):
		for request in self.myrequests:
			if request.res == id:
				print request.files

	def fetch(self,id,filename,output = "output"):
		for request in self.myrequests:
			if request.res == id:
				request.getFile(filename,output)

	def addPackage(self,name,package_path,image_path,description,short_description):
		req = API(URLS["addImg"])
		req.setArgFile(image_path,name,field="image")
		req.send()
		image_id = req.getJson()["res"]
	
		req = API(URLS["addFile"])
		req.setArgFile(package_path,name + "_package")
		req.send()
		file_id = req.getJson()["res"]


		req = API(URLS["addAlgorithm"])
		req.setArgs([
		("name", name),
		("description",str(description)),
		("short_description",str(short_description)),
		("image", str(image_id)),
		("package__zip", str(file_id))
		])
		req.send()

		self.__init__(self.username,self.password)

URLS = {
"listAlgorithm" : "science/algorithm/list/",
"addJob" : "compute/algorithmrunorder/add/",
"viewJob" : "compute/algorithmrunorder/view/",
"getResult" : "	compute/result/",
"addFile" : "references/file/add/",
"addAlgorithm" : "science/algorithm/add/",
"addImg" : "references/image/add/"
}

Types = {"bool":bool,"int":int,"float":float,"str":str,"numpy.ndarray":numpy.ndarray,"file":file}

"""wideio.TEST.createRequest()
wideio.TEST.request.image_input = "/home/valentin/pro/wideio-opencv-packages/media/opencv-logo.png"
wideio.TEST.request.hessianThreshold = 400
wideio.TEST.request.execute()

wideio.myrequests[0].isReady()"""
#wideio.myrequests[0].getFile("image.png","img.png")


#upgrade preprod and test web server
#upgradeprod.sh

#TODO:
# permission denied
# parameters
