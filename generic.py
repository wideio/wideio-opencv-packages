import simplejson, bson, sys
import socket
import generic

class NoDefaultValue(Exception):
	name = ""
	def __init__(self,name):
		name = name

	def __str__(self):
		return "ERROR: NoDefaultValue for parameter " + name
	
	
class BadArgumentType(Exception):
	name = ""
	def __init__(self,name):
		name = name

	def __str__(self):
		return "ERROR: BadArgumentType for parameter " + name



class Argument(object):
	name = ""
	value = None

	def __init__(self,arg_type,default_value,name,request):		
		self.name = name

		if (request["parameters"].has_key(self.name)):
			self.value=request["parameters"][self.name]
			if type(self.value) != arg_type:
				raise BadArgumentType(self.name)
		else:
			if default_value == None:
				raise NoDefaultValue(self.name)



def encode_int(i):
	return chr(i&0xFF)+chr((i>>8)&0xFF)+chr((i>>16)&0xFF)+chr((i>>24)&0xFF)

def decode_int(s):
	return ord(s[0])+(ord(s[1])<<8)+(ord(s[2])<<8)+(ord(s[3])<<8)

def run_server(s,function):
    server_running=True
    while server_running:
       amount=decode_int(s.recv(4))
       request=bson.BSON.load(s.recv(amount))
       if request["type"]=="compute":
           reply={"res":function(request)}
       elif request["type"]=="stop_server":
           server_running=False
           reply={"res":"ok"}
       else:
           reply={"res":"error", "error":"unsupported request type : "+ request["type"]}
       reply=bson.BSON.encode(reply)
       s.send(encode_int(len(reply)))
       s.send(reply)
