import simplejson, bson, sys
import numpy
import scipy
import cv2
from generic import Argument,keypoints2dict
import pydoc
import PIL
import PIL.Image
import os
from zipfile import ZipFile

def function(request):
	data = []
	output = []
	serialization = None

	try:
		parameters = simplejson.loads(request["parameters"]) 
	except:
		print "ERROR : Can't decode JSON parameters"
		return None

	try:
		image_output = Argument(bool,False,False,"image_output",parameters)
		cross_circle = Argument(bool,True,False,"cross_circle",parameters)
	except Exception as e:
		print e

	if not os.path.isfile("request/image_input"):
		print "PACKAGE: FILE request/image_input NOT FOUND... EXITING"
		return ""
	img = cv2.imread("request/image_input")

	detector = cv2.FeatureDetector_create("SIFT")
	descriptor = cv2.DescriptorExtractor_create("SIFT")
	 
	keypoints = detector.detect(img)
	keypoints, description = descriptor.compute(img, keypoints)

	if image_output.value:
		#if cross_circle:
		for keypoint in keypoints:
			x =  int(keypoint.pt[0])
			y =  int(keypoint.pt[1])
				
			for i in range(-2,3):
				try:
					img[y + i][x][0] = 0
					img[y + i][x][1] = 0
					img[y + i][x][2] = 255
				except:
					pass
			for i in range(-2,3):
				try:
					img[y][x + i][0] = 0
					img[y][x + i][1] = 0
					img[y][x + i][2] = 255
				except:
					pass
	
		cv2.imwrite("request/output.png",img)
	
	data.append(keypoints2dict(keypoints))
	data.append(description.tolist())
	
	return simplejson.dumps(data)

if __name__=="__main__":
	mode="request"
	if (sys.argv>1):
		mode=sys.argv[1]
		if mode=="request":
			request=bson.BSON(open("request/request.bson").read()).decode()
			data = function(request)
			if data != None:
				with open('request/output.json', 'w') as outfile:
					outfile.write(data)
				
				with ZipFile('request/output.zip', 'w') as myzip:
					myzip.write('request/output.json')
					if os.path.isfile('request/output.png'):
						myzip.write('request/output.png')
		if mode=="server":
			port = int (sys.argv[2])
			s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			s.connect(("127.0.0.1", port))
			run_server(s,function)
