import simplejson, bson, sys
import numpy
import scipy
import cv2
from generic import Argument,keypoints2dict
import pydoc
import PIL
import PIL.Image
import os
from zipfile import ZipFile

def function(request):
	data = []
	output = []
	serialization = None

	try:
		parameters = simplejson.loads(request["parameters"]) 
	except:
		print "ERROR : Can't decode JSON parameters"
		return None

	try:
		image_output = Argument(bool,False,False,"image_output",parameters)
	except Exception as e:
		print e

	if not os.path.isfile("request/image_input"):
		print "PACKAGE: FILE request/image_input NOT FOUND... EXITING"
		return ""
	img = cv2.imread("request/image_input")

	output = cv2.erode(img,cv2.getStructuringElement(cv2.MORPH_CROSS,(3,3)))

	cv2.imwrite("request/output.png",output)

	return simplejson.dumps({})

if __name__=="__main__":
	mode="request"
	if (sys.argv>1):
		mode=sys.argv[1]
		if mode=="request":
			request=bson.BSON(open("request/request.bson").read()).decode()
			data = function(request)
			if data != None:
				with open('request/output.json', 'w') as outfile:
					outfile.write(data)
				
				with ZipFile('request/output.zip', 'w') as myzip:
					myzip.write('request/output.json')
					if os.path.isfile('request/output.png'):
						myzip.write('request/output.png')
		if mode=="server":
			port = int (sys.argv[2])
			s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			s.connect(("127.0.0.1", port))
			run_server(s,function)
