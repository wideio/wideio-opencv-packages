import simplejson, bson, sys
import socket
import generic
import numpy
import scipy
import tables
import os
import glob

class NoDefaultValue(Exception):
	name = ""
	def __init__(self,name):
		self.name = name

	def __str__(self):
		return "ERROR: NoDefaultValue for parameter " + self.name
	
	
class BadArgumentType(Exception):
	name = ""
	def __init__(self,name):
		self.name = name

	def __str__(self):
		return "ERROR: BadArgumentType for parameter " + self.name

class Argument(object):
	def __init__(self,arg_type,default_value,required,name,params):
		self.name = name
		self.arg_type = arg_type
		self.value = default_value

		if (params.has_key(self.name)):
			self.save_value(params[self.name])
		elif required == True:
			raise NoDefaultValue(self.name)
		
		#array case
		if self.arg_type == "array":
			if os.path.isfile("request/"+name+".mat"):
				self.value = scipy.io.loadmat("request/"+name+".mat")[self.value]
			elif os.path.isfile("request/"+name+".npy"):
				self.value = numpy.load("request/"+name+".npy")
			elif os.path.isfile("request/"+name+".h5"):
				f=tables.open_file("request/"+"test.h5")
				self.array_path = self.value
				node = f.getNode(self.array_path)
	def save_value(self,value):
		try:
			if self.arg_type == "array":
				return
			elif self.arg_type == bool:
				self.value = bool(value)
			elif self.arg_type == int:
				self.value = int(value)
			elif self.arg_type == float:
				self.value = float(value)
			else:
				self.value = value
		except:
			raise BadArgumentType(self.name)

def load_table():
	pass

def keypoints2dict(keypoints):
	data = []
	pattern = {"x":0,"y":0,"dir_degree":1,"hessian":0,"size":0,"laplacian":0}
	for keypoint in keypoints:
		tmp = pattern
		tmp["x"] = keypoint.pt[0]
		tmp["y"] = keypoint.pt[1]
		tmp["dir_degree"] = keypoint.angle
		tmp["size"] = keypoint.size
		tmp["laplacian"] =  round(keypoint.class_id)
		tmp["hessian"] =  keypoint.response
		data.append(tmp)
	return data

def encode_int(i):
	return chr(i&0xFF)+chr((i>>8)&0xFF)+chr((i>>16)&0xFF)+chr((i>>24)&0xFF)

def decode_int(s):
	return ord(s[0])+(ord(s[1])<<8)+(ord(s[2])<<8)+(ord(s[3])<<8)

def run_server(s,function):
    server_running=True
    while server_running:
       amount=decode_int(s.recv(4))
       request=bson.BSON.load(s.recv(amount))
       if request["type"]=="compute":
           reply={"res":function(request)}
       elif request["type"]=="stop_server":
           server_running=False
           reply={"res":"ok"}
       else:
           reply={"res":"error", "error":"unsupported request type : "+ request["type"]}
       reply=bson.BSON.encode(reply)
       s.send(encode_int(len(reply)))
       s.send(reply)
